--Remove a previously inserted film from the inventory and all corresponding rental records
DELETE FROM rental
WHERE inventory_id IN (
    SELECT inventory_id
    FROM inventory
    WHERE film_id = 1004
);

DELETE FROM inventory
WHERE film_id = 1004;